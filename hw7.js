var studentsAndPoints = [
    'Алексей Петров', 0,
    'Ирина Овчинникова', 60,
    'Глеб Стукалов', 30,
    'Антон Павлович', 30,
    'Виктория Заровская', 30,
    'Алексей Левенец', 70,
    'Тимур Вамуш', 30,
    'Евгений Прочан', 60,
    'Александр Малов', 0,
];

//1. Создать два массива students и points и заполнить их данными из studentsAndPoints без использования циклов. В первом массиве фамилии студентов, во втором баллы. Индексы совпадают.

var students = studentsAndPoints.filter (function (student) {return typeof(student) == 'string'});

var points = studentsAndPoints.filter (function (points) {return typeof(points) == 'number';});

//2. Вывести список студентов без использования циклов.
console.log('Список студентов:');

students.forEach(function (name, i) {
    console.log('Студент %s набрал %d баллов', name, points[i]);
}); 

//3. Найти студента, набравшего наибольшее количество баллов, и вывести информацию.

var maxPoints, maxIndex;

points.forEach(function (points, i) {
    if (!maxPoints || points > maxPoints) {
        maxPoints = points;
        maxIndex = i;
    }
});

console.log('\nСтудент набравший максимальный балл: %s (%d баллов)', students[maxIndex], maxPoints);

//4. Используя метод массива map увеличить баллы студентам «Ирина Овчинникова» и «Александр Малов» на 30.

var newPoints = points.map(function (newPoints, i) {
   if (students[i] == 'Ирина Овчинникова' || students[i] == 'Александр Малов') {
      return points[i] + 30; 
   } else {
       return points[i] + 0;
   }
});

points = newPoints;

console.log('\nОбновлены баллы:');

students.forEach(function (name, i) {
    console.log('Студент %s набрал %d баллов', name, points[i]);
}); 

//5. реализовать функцию getTop, которое принимает на вход число и возвращает массив в формате studentsAndPoints, в котором соответствующее кол-во студентов с лучшими баллами в порядке убывания кол-ва баллов.

function getTop(num) {
    var sorting = [],
        top;

    students.forEach(function (name, i) {
        sorting.push([students[i], points[i]]);
        sorting.sort(function (b, a) {
            return (a[1] - b[1])
        });
    });

    top = sorting.reduce(function (flat, current) {
        return flat.concat(current); 
    });
    
    top.length = num * 2;
    
    return top;
}

var topStudents = getTop(3);

console.log("\nТоп %d:", topStudents.length / 2);

for (i = 0, imax = topStudents.length; i < imax; i += 2) {
  console.log('%s - %d баллов', topStudents[i], topStudents[i + 1]);
}

var topStudents = getTop(5);

console.log("\nТоп %d:", topStudents.length / 2);

for (i = 0, imax = topStudents.length; i < imax; i += 2) {
  console.log('%s - %d баллов', topStudents[i], topStudents[i + 1]);
}